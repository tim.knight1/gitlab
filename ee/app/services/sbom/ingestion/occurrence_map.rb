# frozen_string_literal: true

module Sbom
  module Ingestion
    class OccurrenceMap
      include Gitlab::Utils::StrongMemoize

      attr_writer :vulnerability_ids
      attr_reader :report_component, :report_source, :vulnerabilities
      attr_accessor :component_id, :component_version_id, :source_id, :occurrence_id, :source_package_id, :uuid

      def initialize(report_component, report_source, vulnerabilities)
        @report_component = report_component
        @report_source = report_source
        @vulnerabilities = vulnerabilities
        @vulnerability_ids = nil
      end

      def to_h
        {
          component_id: component_id,
          component_version_id: component_version_id,
          component_type: report_component.component_type,
          name: report_component.name,
          purl_type: purl_type,
          source_id: source_id, source_type: report_source&.source_type,
          source: report_source&.data,
          source_package_id: source_package_id,
          source_package_name: report_component.source_package_name,
          uuid: uuid,
          version: version
        }
      end

      def version_present?
        version.present?
      end

      def vulnerability_count
        original_vulnerability_ids.count
      end

      def highest_severity
        vulnerabilities_info[:highest_severity]
      end

      # This weird code is here until we remove the
      # `deprecate_vulnerability_occurrence_pipelines` FF. When that
      # FF is removed, the original_* code branch will be deleted and
      # this can be rolled up into a regular attr_accesor
      def vulnerability_ids
        return @vulnerability_ids unless @vulnerability_ids.nil?

        original_vulnerability_ids
      end

      def original_vulnerability_ids
        vulnerabilities_info[:vulnerability_ids]
      end
      strong_memoize_attr :original_vulnerability_ids

      def purl_type
        report_component.purl&.type
      end

      def packager
        report_component&.properties&.packager || report_source&.packager
      end

      def input_file_path
        return image_ref if container_scanning_component? && image_data_present?

        report_source&.input_file_path
      end

      delegate :image_name, :image_tag, to: :report_source, allow_nil: true
      delegate :name, :version, :source_package_name, :ancestors, to: :report_component

      private

      def vulnerabilities_info
        @vulnerabilities.fetch(name, version, input_file_path)
      end
      strong_memoize_attr :vulnerabilities_info

      def image_data_present?
        image_name.present? && image_tag.present?
      end

      def container_scanning_component?
        report_component.properties&.source_type&.to_sym == :trivy
      end

      def image_ref
        "container-image:#{image_name}:#{image_tag}"
      end
    end
  end
end
